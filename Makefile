sql_parse:
	bison -d sql_parse.y
	flex sql_token_parse.l
	cc -g -o $@ sql_parse.tab.c lex.yy.c structures.c

clean:
	rm -rf lex.yy.c sql_parse.tab.* sql_parse
